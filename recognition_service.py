import os
import sys
import numpy as np
from flask import Flask, jsonify, abort, make_response, request
from classifier import build_model, predict_using_classifier

app = Flask(__name__)
model = None


@app.route('/api/v1/recognizeActivity', methods=['POST'])
def recognize_activity():
    if not request.json or 'records' not in request.json:
        abort(400)

    records = request.json['records']

    if 'x' not in records or 'y' not in records or 'z' not in records:
        abort(400)

    # Arrange the input data in the correct data layout.
    x, y, z = np.asarray(records["x"]), np.asarray(records["y"]), np.asarray(records["z"])

    # Perform the prediction.
    result = predict_using_classifier(model, x, y, z)

    # Return the result list.
    return jsonify(results=next(result).tolist())


@app.errorhandler(500)
def internal_error(error):
    return make_response(jsonify({'error': 'Internal server error'}), 500)


@app.errorhandler(404)
def not_found(error):
    return make_response(jsonify({'error': 'Not found'}), 404)

if __name__ == '__main__':
    # Using a learning rate of 0 to prevent model updates during service runtime
    # caused by the workaround employed in __classifier.predict_using_classifier__.
    model = build_model(sys.argv[1] if len(sys.argv) > 1 else "model", learning_rate=0)

    port = int(os.environ.get('PORT', 12000))
    app.run(host='0.0.0.0', port=port, debug=False)
