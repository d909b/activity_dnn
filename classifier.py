import tensorflow as tf
import pandas as pd
import numpy as np
import tempfile
import os
from util import split_set, equal_sized_chunks


tf.logging.set_verbosity(tf.logging.INFO)

flags = tf.app.flags
FLAGS = flags.FLAGS

# Define application flags and default values.
flags.DEFINE_string("model_dir", "", "Base directory for output models.")
flags.DEFINE_integer("train_steps", 1000, "Number of training steps.")
flags.DEFINE_integer("learning_rate", 0.1, "Learning rate of the optimizer.")
flags.DEFINE_integer("p_dropout", 0.5, "Dropout probability during training.")
flags.DEFINE_integer("train_fraction", 0.8, "Fraction of the data set to use for training.")
flags.DEFINE_integer("max_samples", 2000,
                     "Maximum number of samples to use from the training set per class. (0 for no limit)")
flags.DEFINE_integer("num_records_per_sample", 200,
                     "Number of data records per sample.")
flags.DEFINE_string("data", "", "Path to the training data.")

# Each feature group is indexed using an abbreviated name:
#   - avg    ... Average
#   - stddev ... Standard Deviance
#   - aad    ... Absolute Average Distance
#   - ard    ... Absolute Resultant Acceleration
#   - hist   ... Histogram
#
# NOTE: The features are equivalent to the ones used by
# "Activity Recognition using Cell Phone Accelerometers", KDD, 2010, Kwapisz, J. et al.
# except the not sufficiently defined "time between peaks" feature.
#
FEATURE_AVG = "avg"
FEATURE_STDDEV = "stddev"
FEATURE_AAD = "aad"
FEATURE_ARD = "ard"
FEATURE_HIST = "hist"

# Define the feature groups and their respective dimensionality.
# Ex.: "avg": 3 ... defines a feature group "avg" with feature dimension 3.
FEATURES = {FEATURE_AVG: 3, FEATURE_STDDEV: 3, FEATURE_AAD: 3, FEATURE_ARD: 1, FEATURE_HIST: 30}

# Define global settings for data set parsing.
DATASET_CACHE_FILE = "dataset.pickle"
MAPPING_NAME_WALKING = "Walking"
MAPPING_NAME_JOGGING = "Jogging"
MAPPING_CODE_WALKING = 0
MAPPING_CODE_JOGGING = MAPPING_CODE_WALKING + 1
MAPPING_CODE_OTHER = MAPPING_CODE_JOGGING + 1

# Caches the evaluation state for checkpoint reloading. (Workaround for TensorFlow API limitations)
g_is_first_evaluation = True


def get_model_directory():
    """
    Get the model directory to use for storing of checkpoints and model data.

    NOTE: A trained model can later be reused by loading from the same path.
    NOTE: The command line parameter --model_dir can be used to set the model directory.
          Otherwise, a unique destination folder will be created on your system automatically.

    :return: The model directory used to store checkpoints and model data.
    """
    return tempfile.mkdtemp() if not FLAGS.model_dir else FLAGS.model_dir


def build_model(model_dir, learning_rate=0.1):
    """
    Builds a TensorFlow (TF) classification model
    using __model_dir__ to store checkpoints and graph model data.

    :param model_dir: The directory to use to store checkpoints and graph model data.
    :param learning_rate: The initial value for the learning rate employed
                          by the optimisation algorithm.
    :return: An untrained Neural Network (NN) model for classification.
    """
    # Pre-arrange feature columns.
    feature_columns = [tf.contrib.layers.real_valued_column(name, dimension=dim) for name, dim in FEATURES.iteritems()]

    optimizer = tf.train.AdagradOptimizer(learning_rate=learning_rate)
    m = tf.contrib.learn.DNNClassifier(feature_columns=feature_columns,
                                       hidden_units=[32, 32],
                                       n_classes=2,
                                       model_dir=model_dir,
                                       dropout=FLAGS.p_dropout,
                                       activation_fn=tf.nn.relu,
                                       optimizer=optimizer,
                                       config=tf.contrib.learn.RunConfig(save_checkpoints_secs=60))
    return m


def features_from_raw_data(x, y, z):
    """
    Extracts features from raw data lists __x__, __y__ and __z__.

    :param x: A list of acceleration values in the x axis.
    :param y: A list of acceleration values in the y axis.
    :param z: A list of acceleration values in the z axis.
    :return: A dictionary mapping a feature group's abbreviated name
             to the corresponding feature values.
    """
    assert len(x) == len(y)
    assert len(y) == len(z)

    # Define a helper function to extract features from the raw data.
    def apply_fun_to_set(fun):
        return [map(fun, [x[i], y[i], z[i]]) for i in xrange(len(x))]

    # Define a feature extraction function for each feature group indexed by its abbreviated name.
    features = {
        FEATURE_AVG: np.average,
        FEATURE_STDDEV: np.std,
        FEATURE_AAD: lambda vals: np.average(np.absolute(vals - np.average(vals))),
        FEATURE_ARD: np.square,
        FEATURE_HIST: lambda vals: np.histogram(vals, density=True)[0]
    }

    # Extract the features for each feature group.
    features = {
        k: apply_fun_to_set(v) for k, v in features.iteritems()
    }

    hist = features[FEATURE_HIST]

    # Re-arrange Average Resultant Acceleration (ARD) and Histogram (HIST) feature groups to match the expected shapes.
    features[FEATURE_ARD] = np.average(np.sqrt(np.sum(features[FEATURE_ARD], axis=1)), axis=1)
    features[FEATURE_HIST] = [np.array(hist[i]).flatten().tolist() for i in xrange(len(hist))]

    return features


def extract_training_features(df):
    """
    Extract the features and labels for each data point in the
    pandas.DataFrame __df__ consisting of data points (in rows)
    and features (in columns).

    :param df: A pandas.DataFrame with data points in rows and features in columns.
    :return: (features, labels) A set of extracted __features__ (stored in NDArrays, and
                               their corresponding __labels__.
    """
    print("Len is: {}".format(len(df[3].values)))

    # Split the measurements into equally-sized blocks corresponding to a specific time window.
    x, y, z, labels = map(lambda l: equal_sized_chunks(l, FLAGS.num_records_per_sample),
                          (df[3].values, df[4].values, df[5].values, df[1].values))

    return features_from_raw_data(x, y, z), labels


def input_transform(df):
    """
    Transforms the input data to feature space before training, prediction and evaluation.

    :param df: The data batch stored in a pandas.DataFrame.
               Rows correspond to data points and columns correspond to features.
    :return: (features, labels) A set of extracted __features__ (stored in tf.constant nodes, and
                               their corresponding __labels__.
    """
    # Extract features from the raw data in __df__.
    features, labels = extract_training_features(df)

    # Determine the number of data points (=rows) in __df__.
    num_rows = len(df.index) / FLAGS.num_records_per_sample

    # Define a mapping from each feature column to
    # the values of that column stored in a constant tensor node.
    features = {
        name: tf.constant(value=features[name], name=name, shape=[num_rows, dim])
        for name, dim in FEATURES.iteritems()
    }

    # Converts the label column into a constant tensor.
    label = tf.constant([l[0] for l in labels], name="target", shape=[num_rows, 1])

    return features, label


def load_dataset():
    """
    Loads a data set from disk that can be used for training the classifier.

    NOTE: The data set can be obtained from: http://www.cis.fordham.edu/wisdm/dataset.php#actitracker

    CREDITS:
             Gary M. Weiss and Jeffrey W. Lockhart (2012).
             "The Impact of Personalization on Smartphone-Based Activity Recognition,"
             Proceedings of the AAAI-12 Workshop on Activity Context Representation: Techniques and Languages,
             Toronto, CA.

             Jennifer R. Kwapisz, Gary M. Weiss and Samuel A. Moore (2010).
             "Activity Recognition using Cell Phone Accelerometers,"
             Proceedings of the Fourth International Workshop on Knowledge Discovery from Sensor Data (at KDD-10),
             Washington DC.

    :return: The loaded data set in a pandas.DataFrame with rows corresponding to data points and columns
             corresponding to features.
    """
    # Define a mapping from class names to integer indices.
    mapping = {MAPPING_NAME_WALKING: 0,
               MAPPING_NAME_JOGGING: 1}

    # Define a converter mapping the class name column to the correct integer indices.
    # NOTE: Any class not in the __mapping__ is indexed with __MAPPING_CODE_OTHER__
    converters = {1: lambda val: mapping[val] if val in mapping else MAPPING_CODE_OTHER}

    if os.path.isfile(DATASET_CACHE_FILE):
        # Load the data set from cache if possible
        # which is more performant than (re-)parsing the CSV file on subsequent runs.
        dataset = pd.read_pickle(DATASET_CACHE_FILE)
    else:
        dataset = pd.read_csv(
            FLAGS.data,
            header=None,
            skipinitialspace=True,
            usecols=range(1, 6),
            delimiter=",",
            lineterminator=";",
            encoding="utf8",
            converters=converters,
            engine="c"
        )

        # Cache the parsed data set for a faster start-up time on subsequent runs.
        dataset.to_pickle(DATASET_CACHE_FILE)

    # Filter the data set by class. We are only interested in walking and jogging (=running).
    d_walk = dataset[(dataset[1] == mapping[MAPPING_NAME_WALKING])]
    d_run = dataset[(dataset[1] == mapping[MAPPING_NAME_JOGGING])]

    # Adhere to the parameter setting the maximum number of samples to take per class.
    # This parameter is used to ensure the distribution of classes in the training data is balanced.
    if FLAGS.max_samples != 0:
        num_records_per_class = int(FLAGS.max_samples*FLAGS.num_records_per_sample)

        print("Walking: {}/{}, Running: {}/{}".format(len(d_walk.index), num_records_per_class,
                                                      len(d_run.index), num_records_per_class))

        if num_records_per_class < len(d_walk.index):
            d_walk = d_walk[:num_records_per_class]
        else:
            print("Not enough walking records available to limit at {}.".format(num_records_per_class))

        if num_records_per_class < len(d_run.index):
            d_run = d_run[:num_records_per_class]
        else:
            print("Not enough running records available to limit at {}.".format(num_records_per_class))

    # Split the data set into training and test data.
    d_walk_train, d_walk_test = split_set(d_walk, FLAGS.train_fraction)
    d_run_train, d_run_test = split_set(d_run, FLAGS.train_fraction)

    # Recombine the per-class data sets into combined data sets for training and testing.
    return d_walk_train.append(d_run_train), d_walk_test.append(d_run_test)


def train_classifier(classifier, dataset_train, dataset_test, steps=FLAGS.train_steps):
    """
    Trains a __classifier__ using a training set __dataset_train__ and a test set __dataset_test__.

    NOTE: The training parameters are set using command line parameters.

    :param classifier: A preconfigured TensorFlow classifier object.
    :param dataset_train: A data set used for training
    :param dataset_test: A data set used for testing
    :param steps: The number of steps to train for.
    :return: A trained TensorFlow classifier.
    """
    metrics = {("accuracy", "classes"): tf.contrib.metrics.streaming_accuracy}

    # Employ a validation monitor on the test data set for early stopping.
    validation_monitor = tf.contrib.learn.monitors.ValidationMonitor(
        input_fn=lambda: input_transform(dataset_test),
        every_n_steps=100, early_stopping_rounds=200, metrics=metrics, eval_steps=1
    )

    # Fit the classifier's parameters to the given training data set.
    classifier.fit(input_fn=lambda: input_transform(dataset_train),
                   steps=steps,
                   monitors=[validation_monitor])


def evaluate_classifier(classifier, dataset_test):
    """
    Evaluates __classifier__ on the test set __dataset_test__.
    """
    results = classifier.evaluate(input_fn=lambda: input_transform(dataset_test), steps=1)

    # Print the results to the console.
    for key in sorted(results):
        print("%s: %s" % (key, results[key]))


def predict_using_classifier(classifier, x, y, z):
    """
    Performs a prediction on __x__, __y__ and __z__ using ___classifier__.
    """
    global g_is_first_evaluation

    # Padding for the data frame to fit the same format as the training data.
    # This has no impact on prediction performance but saves an additional data transformation.
    padding = [0]*len(x)

    # Create a data frame of the same format as given in the training data.
    df = pd.DataFrame.from_dict({1: padding, 2: padding, 3: x, 4: y, 5: z})

    # Due to TensorFlow API limitations,
    # a classifier that has been restored from a previous checkpoint currently needs to be
    # trained for at least one step in order for the prediction to work correctly.
    if g_is_first_evaluation:
        train_classifier(classifier, df, df)
        g_is_first_evaluation = False

    # Perform a prediction on the created data frame using the same input transform used for training.
    return classifier.predict_proba(input_fn=lambda: input_transform(df)[0], as_iterable=True)


if __name__ == '__main__':
    def main(_):
        # Load the data set from disk and split it into a test and training subset.
        df_train, df_test = load_dataset()

        # Determine the directory that stores the model and its checkpoints.
        model_dir = get_model_directory()
        print("model directory = %s" % model_dir)

        # Create the model.
        classifier = build_model(model_dir, FLAGS.learning_rate)

        # Train the model on the training data set.
        train_classifier(classifier, dataset_test=df_test, dataset_train=df_train)

        # Evaluate the model on the test data set.
        evaluate_classifier(classifier, dataset_test=df_test)

if __name__ == "__main__":
    # Run using TF's app functionality (for argument parsing).
    tf.app.run()
