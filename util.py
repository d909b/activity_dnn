import pandas as pd


def split_set(data, fraction):
    """
    Splits a data set into a training set of size
    __fraction__ * number of data points and a test set of
    (1 - __fraction__) * number of data points.

    :param data: A pandas.DataFrame containing the full data set.
                 (Rows are data points and columns are features)
    :param fraction: The __fraction__ of the data set to use for training. [0, 1]
    :return: (dataset_train, dataset_test) - A __dataset_train__ and __dataset_test__
                                             as a result of splitting the original data set
                                             at the specified sizes.
    """
    fraction = max(min(fraction, 1), 0)

    num_rows = len(data.index)
    cut_idx = int(num_rows*fraction)
    d_train = data[:cut_idx]
    d_test = data[cut_idx:]
    return d_train, d_test


def split_multidimensional_arrays(features):
    """
    Splits multi-dimensional sub-lists in the list of lists __features__
    into multiple sub-columns with each sub-column corresponding to a feature dimension.

    Multi-dimensional feature groups are additionally indexed by a numbered suffix.
    Ex.: avg:1 .. The first dimension of the "avg" feature.

    NOTE: This method is used to save to the CSV format using the utility method provided
          by pandas.DataFrame.

    :param features: A 2D list organised by data points (row-first)
    :return: A pandas.DataFrame organised by named features (column-first)
    """
    for k in features.keys():
        v = features[k]

        # Only multi-dimensional features need to be reorganised.
        if isinstance(v[0], list):
            inverted = zip(*v)

            # Create sub-columns for each feature dimension.
            for i in xrange(len(inverted)):
                features["{}:{}".format(k, i)] = inverted[i]

            # Delete the original multi-dimensional column.
            del features[k]

    return features


def save_to_csv(features, name):
    """
    Saves a feature set __features__ to a .CSV file using the filename __name__.

    :param features: A feature set (dictionary of named arrays, each corresponding to a feature group)
    :param name: The filename to use for the created .CSV file.
    """
    pd.DataFrame.from_dict(split_multidimensional_arrays(features)).T.to_csv(name, index=False)


def equal_sized_chunks(l, n):
    """
    Splits a given list __l__ into __n__ equal sized chunks.

    NOTE: Used to extract the measurement groups corresponding to
    a given time window.

    :param l: A list to be split into equal sized chunks.
    :param n: The number of equal sized chunks to return.
    :return: The list __l__ split into a list of __n__ equally sized lists.
    """
    assert len(l) % n == 0
    return [l[i:i + n] for i in xrange(0, len(l), n)]